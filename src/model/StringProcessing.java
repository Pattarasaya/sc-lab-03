package model;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class StringProcessing {
	public String text =  "";

	public void wordTokenizer(String str){	
		StringTokenizer st = new StringTokenizer(str);
		ArrayList<String> list = new ArrayList<String>();
		int word = st.countTokens();
		while (st.hasMoreTokens()) {
			list.add(st.nextToken());
		}
		for(int i = 0; i < word; i++){ 
			text += list.get(i);
		}
		System.out.println("Word = " + word);
	}

	public void ngramTokenizer(int n){
		try{
			for(int i = 0; i < text.length(); i++){
				System.out.print(text.substring(i,i+n) + " ");
			}
		} catch (Exception e){
		}
	}
}
