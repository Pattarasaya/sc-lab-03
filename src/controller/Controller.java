package controller;
//package controller;
//
//import java.awt.BorderLayout;
//import java.awt.EventQueue;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//
//import javax.swing.ButtonGroup;
//import javax.swing.JButton;
//import javax.swing.JFrame;
//import javax.swing.JLabel;
//import javax.swing.JPanel;
//import javax.swing.JRadioButton;
//import javax.swing.JTextArea;
//import javax.swing.JTextField;
//import javax.swing.border.TitledBorder;
//
//import model.StringProcessing;
//
//public class Control {
//	private JFrame frame;
//	private StringProcessing strpro;
//	private String state = "ngram";
//	private JTextField textNgramInput;
//	private JTextArea textarea;
//	private JPanel rightpanel;
//	private JPanel optionpanel;
//	private JRadioButton rdbtSpace;
//	private JRadioButton rdbtNgram;
//	private JButton btSummit;
//	private JLabel lbNgramInput;
//	private JPanel resultpanel;
//	private ButtonGroup btGroup;
//	private JTextArea arearesult;
//	
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				try{
//					
//					
//				}finally{
//					
//			}
//			}
//		});
//	}
//
//
//	public Control(StringProcessing strprocess) {
//		strpro = strprocess;
//		initialize();
//	}
//
//	private void initialize(){
//		frame = new JFrame();
//		frame.setBounds(100, 300, 800, 300);
//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.setVisible(true);
//		textarea = new JTextArea(10,30);
//		frame.getContentPane().add(textarea, BorderLayout.WEST);
//
//		rightpanel = new JPanel();
//		frame.getContentPane().add(rightpanel, BorderLayout.CENTER);
//		rightpanel.setLayout(new BorderLayout(0,0));
//
//		optionpanel = new JPanel();
//		optionpanel.setBorder(new TitledBorder(null, "Option: "));
//		rightpanel.add(optionpanel, BorderLayout.SOUTH);
//
//		rdbtSpace = new JRadioButton("Split Space",true);
//		optionpanel.add(rdbtSpace);
//		rdbtSpace.addActionListener(new ActionListener() {
//
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				// TODO Auto-generated method stub
//				state = "space";
//				textNgramInput.setVisible(false);
//				lbNgramInput.setVisible(false);
//			}
//		});
//
//		rdbtNgram = new JRadioButton("nGram",false);
//		optionpanel.add(rdbtNgram);
//		rdbtNgram.addActionListener(new ActionListener() {
//
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				// TODO Auto-generated method stub
//				state = "nGram";
//				textNgramInput.setVisible(true);
//				lbNgramInput.setVisible(true);
//			}
//		});
//
//		btSummit = new JButton("Summit");
//		btSummit.addActionListener(new ActionListener() {
//
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				// TODO Auto-generated method stub
//				if (state.equals("space")) {
//					strpro.wordTokenizer(textarea.getText());	
//				}
//				else if (state.equals("nGram")) {
//					int n;
//					try{
//						n = Integer.parseInt(textNgramInput.getText());
//						strpro.ngramTokenizer(n);
//					} 
//					catch (Exception ex) {
//						// TODO: handle exception
//					}
//				}
//			}
//		});
//		textNgramInput.setColumns(10);
//		textNgramInput.setVisible(false);
//		optionpanel.add(btSummit);
//
//		resultpanel = new JPanel();
//		resultpanel.setBorder(new TitledBorder(null, "Result"));
//		rightpanel.add(resultpanel, BorderLayout.CENTER);
//		resultpanel.setLayout(new BorderLayout(0,0));
//
//		arearesult = new JTextArea();
//		arearesult.setEditable(true);
//		resultpanel.add(arearesult, BorderLayout.CENTER);
//
//		btGroup = new ButtonGroup();
//		btGroup.add(rdbtSpace);
//		btGroup.add(rdbtNgram);
//
//	}
//
//	public void setResult(String result){
//		arearesult.setText(result);
//	}
//}
