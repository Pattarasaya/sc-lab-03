package view;

import javax.swing.JOptionPane;

import model.StringProcessing;

public class View {
	
	public static void main(String[] args) {
		
		String str = JOptionPane.showInputDialog("Enter Word: ");
		System.out.println(str);
		String in = JOptionPane.showInputDialog("n = ");
		int n = Integer.parseInt(in);
		System.out.println("n = "+ n);
		StringProcessing process = new StringProcessing();
		process.wordTokenizer(str);
		process.ngramTokenizer(n);
	}	
}
